# README #
## Crowdcube Tech Test ##


### Description ###

This is the README for the **Crowdcube Tech Test** repository.
It will guide you through the setup of the project on your own machine.

### How do I get set up? ###

* Clone into this repository using `git clone https://bitbucket.org/rmb32/crowdcube.git`
* Through your terminal, change into the project directory
* Install the required dependencies by running `npm install`
* Ignore any minor warnings
* Start the development server by running `npm run start`
* When the server starts just navigate your browser to `http://localhost:8080/`
* Have fun!

### Main technologies used ###

* HTML5
* CSS3
* JavaScript ES5 & ES6
* React
* Webpack

### Who do I talk to? ###

For any issues or questions contact Roger: roger.barnfather@gmail.com