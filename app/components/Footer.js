import React from 'react';

class Footer extends React.Component {
	render() {
		return (
			<footer className="rb-footer">
				<h4>Risk warning</h4>
				
				<p>
					Investing in start-ups and early stage businesses involves risks, including illiquidity, lack of dividends, loss of investment and dilution, and it should be done only as part of a diversified portfolio. Crowdcube is targeted exclusively at investors who are sufficiently sophisticated to understand these risks and make their own investment decisions. You will only be able to invest via Crowdcube once you are registered as sufficiently sophisticated.
				</p>
				
				<p>
					Please click here to read the <a href="#">full risk warning</a>. This page is approved as a financial promotion by Crowdcube Capital Limited, which is authorised and regulated by the Financial Conduct Authority. Pitches for investment are not offers to the public and investments can only be made by members of crowdcube.com on the basis of information provided in the pitches by the companies concerned. Crowdcube takes no responsibility for this information or for any recommendations or opinions made by the companies.
				</p>
				
				<p>
					The availability of any tax relief, including EIS and SEIS, depends on the individual circumstances of each investor and of the company concerned, and may be subject to change in the future. If you are in any doubt about the availability of any tax reliefs, or the tax treatment of your investment, you should obtain independent tax advice before proceeding with your investment.
				</p>
				
				<div className="rb-footer-social">
					<img src="https://static-crowdcube-com.s3.amazonaws.com/footer/app-store-badge_en-97afa199bcc7ca71bbd549d70fca69e5.svg" alt="Download on the App Store" title="Download on the App Store" />
					
					<a href="#" className="rb-footer-social-links"><img src="/images/social.png" alt="Social media links" /></a>
				</div>
				
				<nav className="rb-footer-nav">
					<ul>
						<li>
							<ul>
								<li>Support</li>
								<li><a href="#">Help centre</a></li>
								<li><a href="#">Contact us</a></li>
								<li><a href="#">Tax relief</a></li>
							</ul>
						</li>
						
						<li>
							<ul>
								<li>Learn more</li>
								<li><a href="#">Raising finance</a></li>
								<li><a href="#">Funded companies</a></li>
								<li><a href="#">Partnerships</a></li>
							</ul>
						</li>
						
						<li>
							<ul>
								<li>Legal</li>
								<li><a href="#">Terms of use</a></li>
								<li><a href="#">Privacy policy</a></li>
								<li><a href="#">Risk warning</a></li>
							</ul>
						</li>
						
						<li>
							<ul>
								<li>Team</li>
								<li><a href="#">About us</a></li>
								<li><a href="#">Careers</a></li>
							</ul>
						</li>
					</ul>
				</nav>
				
				<p>
					Crowdcube Capital Ltd is authorised and regulated by the Financial Conduct Authority (No. 650205).
				
					<a href="#" className="rb-footer-branding">
						<img src="/images/branding.svg" alt="logo" />
					</a>
				</p>
			</footer>
		);
	}
}

export default Footer;