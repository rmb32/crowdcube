import React from 'react';
 
class DaysLeft extends React.Component {
	render() {
		const date1 = new Date(this.props.expires);
		const date2 = new Date();
		const timeDiff = Math.abs(date2.getTime() - date1.getTime());
		const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		
		let message = '';
		
		if(date1 < date2) {
			message = 'Expired ' + diffDays + ' day' + (diffDays > 1 && 's') + ' ago';
		} else {
			message = diffDays + ' day' + (diffDays > 1 && 's') + ' left';
		}
		
		return <span>{ message }</span>
	}
}

export default DaysLeft;