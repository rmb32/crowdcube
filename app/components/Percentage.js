import React from 'react';
 
class Percentage extends React.Component {
	render() {
		const value = this.props.value;
		const style = {
			width: value + '%',
			color: 'white',
			background: value >= 50 ? '#64a816' : '#f08f00'
		};
		
		return (
			<div className={ this.props.styleName && this.props.styleName }>
				<div style={ style }>{ value + '%' }</div>
			</div>
		);
	}
}

export default Percentage;