import React from 'react';

class RiskPanel extends React.Component {
	render() {
		return (
			<div className="rb-risk-panel">
				<select id="rb-new-select">
					<option>Most recent investment</option>
					<option>Newest</option>
					<option>Persentage raised</option>
					<option>Amount raised</option>
				</select>
				
				<div className="rb-risk-box">
					Capital at Risk. Please read our <a href="#">risk warning</a> and <a href="#">disclaimer</a>
				</div>
			</div>
		);
	}
}

export default RiskPanel;