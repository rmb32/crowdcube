import React from 'react';

class FilterBar extends React.Component {
	constructor(props) {
		super(props);
		
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick() {
		this.props.toggleFilterPanel();
	}
	
	render() {
		return (
			<div className="rb-filter-bar">
				<button onClick={ this.handleClick }>
					{ 'Filter (' + this.props.filtersActive + ')' }
					
					<span style={{ float:'right' }}>{ this.props.sign }</span>
				</button>
			</div>
		);
	}
}

export default FilterBar;