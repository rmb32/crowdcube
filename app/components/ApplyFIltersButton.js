import React from 'react';

class ApplyFiltersButton extends React.Component {
	constructor(props) {
		super(props);
		
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick(e) {
		this.props.applyFilters();
	}
	
	render() {
		return <button
			className="rb-apply-filters"
			onClick={ this.handleClick }>Apply filters</button>
	}
}

export default ApplyFiltersButton;