import React from 'react';
import Currency from 'react-currency-formatter';
import DaysLeft from './DaysLeft.js';
import Percentage from './Percentage.js';

class Opportunity extends React.Component {
	render() {
		const op = this.props.opportunity;
		const coverStyle = {
			backgroundImage: 'url(' + op.images[1].src + ')'
		};
		const pounds1 = parseInt(op.investment.target[0] / 100);
		const pounds2 = parseInt(op.investment.current[0] / 100);
		
		return (
		<div className="rb-opportunity">
			<a href="#">
				<div className="rb-opportunity-cover" style={ coverStyle }></div>
				
				<div className="rb-opportunity-top">
					<img src={ op.images[0].src } alt="logo" />
					
					<abbr title="Enterprise Investment Scheme">EIS</abbr>
					
					<DaysLeft expires={ op.expires_at } />
				</div>
				
				<div className="rb-opportunity-middle">
					<h4>{ op.name }</h4>
					
					<p>{ op.description }</p>
				</div>
				
				<div className="rb-opportunity-bottom">
					<Currency
						quantity={ pounds1 }
						currency={ op.investment.target[1] }
						pattern="!##,### "
					/> Target
					
					<Percentage
						value={ op.investment.percentage }
						styleName="rb-opportunity-percentage"
					/>
					
					<div className="rb-opportunity-figures">
						<div>
							<Currency
								quantity={ pounds2 }
								currency={ op.investment.current[1] }
								pattern="!##,### "
							/>
							<span>Raised</span>
						</div>
						
						<div>{ op.equity + '%' }<span>Equity</span></div>
						
						<div>21<span>Investors</span></div>
					</div>
				</div>
			</a>
			
			<a href="#" className="rb-opportunity-favourite">
				<svg version="1.1" x="0" y="0" width="20" height="20" viewBox="0, 0, 32, 32">
					<path className="rb-opportunity-star" d="M25.6,30.6 L16,25.5 L6.4,30.6 L8.2,19.9 L0.4,12.3 L11.2,10.7 L16,1 L20.8,10.8 L31.6,12.4 L23.8,20 L25.6,30.6 z"></path>
				</svg>
			</a>
			
			{ op.investment.current[0] > op.investment.target[0] &&
				<div className="rb-opportunity-sash">OVERFUNDING</div>
			}
		</div>
		);
	}
}

export default Opportunity;