import React from 'react';

class FilterButtonSet extends React.Component {
	constructor(props) {
		super(props);
		
		this.handleChange = this.handleChange.bind(this);
	}
	
	handleChange(e) {
		this.props.toggle(parseInt(e.target.id));
	}
	
	render() {
		const filterType = this.props.type;
		
		const filterButtons = this.props.filterButtons.filter(button => (
			button.type === filterType
		));
		
		const title = filterType.substr(0, 1).toUpperCase() + filterType.substr(1).toLowerCase();
		
		return (
			<div>
				<h4>{ title }</h4>
				
				<ul>
					{ filterButtons.map((filter) => (
						<li key={filter.id}>
							<input type="checkbox"
								id={ filter.id }
								checked={ filter.selected }
								onChange={ this.handleChange } />
							
							<label htmlFor={ filter.id }>{ filter.name }</label>
						</li>
					))}
				</ul>
			</div>
		);
	}
}

export default FilterButtonSet;