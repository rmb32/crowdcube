import React from 'react';

class ResetFiltersButton extends React.Component {
	constructor(props) {
		super(props);
		
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick(e) {
		this.props.resetFilters();
	}
	
	render() {
		return <button
			className="rb-reset-filters"
			onClick={ this.handleClick }>
				Clear filters
			</button>
	}
}

export default ResetFiltersButton;