import React from 'react';

class SearchBar extends React.Component {
	constructor() {
		super();
		
		this.state = { searchTerm: '' };
		
		this.handleChange = this.handleChange.bind(this);
	}
	
	handleChange(e) {
		this.props.applySearch(e.target.value);
		
		this.setState({ searchTerm: e.target.value });
	}
	
	render() {
		return (
			<div className="rb-search-bar">
				<input type="text"
					placeholder="Search"
					onChange={ this.handleChange }
					value={ this.state.searchTerm } />
				
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" width="21.95" height="22.33" viewBox="0, 0, 21.95, 22.33" style={{ float:'right' }}>
					<g>
						<path d="M12.97,2.16 C9.247,-0.073 4.418,1.136 2.186,4.859 C-0.046,8.583 1.163,13.411 4.887,15.643 C8.611,17.874 13.439,16.664 15.67,12.94 C17.894,9.217 16.687,4.395 12.97,2.16 M0.59,5.73 C1.939,2.224 5.336,-0.065 9.092,0.002 C12.848,0.068 16.162,2.476 17.386,6.027 C18.61,9.579 17.483,13.517 14.566,15.883 C11.648,18.25 7.563,18.54 4.34,16.61 C0.601,14.385 -0.984,9.786 0.59,5.73" fill="#313B3E"></path>
						<path d="M13.18,15.64 L13.66,15.28 C14.251,14.833 14.777,14.304 15.22,13.71 L15.6,13.16 L21.49,19.42 C21.957,19.861 22.139,20.526 21.963,21.143 C21.786,21.761 21.28,22.229 20.651,22.356 C20.021,22.484 19.373,22.25 18.97,21.75 z" fill="#313B3E"></path>
					</g>
				</svg>
			</div>
		);
	}
}

export default SearchBar;