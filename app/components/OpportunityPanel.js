import React from 'react';
import Opportunity from './Opportunity.js';

class OpportunityPanel extends React.Component {
	constructor(props) {
		super(props);
	}
	
	render() {
		return (
			<div className="rb-opportunity-panel">
			{ this.props.opportunities.map(op => (
				<Opportunity key={op.id} opportunity={op} />
			))}
			
			{ this.props.opportunities.length === 0 &&
				<p className="rb-no-investments">No matching investments were found</p>
			}
			</div>
		);
	}
}

export default OpportunityPanel;