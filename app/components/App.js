import React from 'react';
import SearchBar from './SearchBar.js';
import FilterBar from './FilterBar.js';
import FilterPanel from './FilterPanel.js';
import OpportunityPanel from './OpportunityPanel.js';
import RiskPanel from './RiskPanel.js';
import Footer from './Footer.js';

var opportunities = require('./crowdcube-test-data.json');
console.log(opportunities);


class App extends React.Component {
    constructor() {
		super();
		
		this.state = {
			filterPanelShow: false,
			opportunities: this.getDefaultOpportunities(),
			filters: this.getDefaultFilters()
		};
		
		this.toggleFilterPanel = this.toggleFilterPanel.bind(this);
		this.applySearch = this.applySearch.bind(this);
		this.toggleFilter = this.toggleFilter.bind(this);
		this.applyFilters = this.applyFilters.bind(this);
		this.resetFilters = this.resetFilters.bind(this);
	}
	
	getDefaultOpportunities() {
		return opportunities;
	}
	
	getDefaultFilters() {
		return [
			{ id: 1, name: 'Seed', selected: false, type: 'stage' },
			{ id: 2, name: 'Early', selected: false, type: 'stage' },
			{ id: 3, name: 'Growth', selected: false, type: 'stage' },
			{ id: 4, name: 'Automotive', selected: false, type: 'sector' },
			{ id: 5, name: 'Business services', selected: false, type: 'sector' },
			{ id: 6, name: 'Customer goods', selected: false, type: 'sector' },
			{ id: 7, name: 'Fintech', selected: false, type: 'sector' },
			{ id: 8, name: 'Food & beverage (FMCG)', selected: false, type: 'sector' },
			{ id: 9, name: 'Heathtech & healthcare', selected: false, type: 'sector' },
			{ id: 10, name: 'IT & telecommunications', selected: false, type: 'sector' },
			{ id: 11, name: 'Leisure and tourism', selected: false, type: 'sector' },
			{ id: 12, name: 'Manufacturing', selected: false, type: 'sector' }
		];
	}
	
	toggleFilterPanel() {
		this.setState({ filterPanelShow: !this.state.filterPanelShow });
	}
	
	toggleFilter(id) {
		const filters = this.state.filters.map(f => {
			if(f.id === id) {
				f.selected = !f.selected;
			}
			
			return f;
		});
		
		this.setState({ filters });
	}
	
	applySearch(term) {
		let reducedOpportunities = this.getDefaultOpportunities().filter(op => {
			const name = op.name.toLowerCase();
			const description = op.description.toLowerCase();
			const combined = name + ' ' + description;
			
			return combined.indexOf(term.toLowerCase()) !== -1;
		});
		
		this.setState({ opportunities: reducedOpportunities });
	}
	
	applyFilters() {
		console.log('Yet to be implemented');
	}
	
	resetFilters() {
		this.setState({ filters: this.getDefaultFilters() });
	}
	
    render() {
		const filtersActive = this.state.filters.filter(f => (f.selected)).length;
		
        return (
			<div>
				<div className="rb-title-bar">
					<h2>Investment Opportunities</h2>
				</div>
				
				<div className="rb-title-bars">
					<FilterBar
						filtersActive={ filtersActive }
						toggleFilterPanel={ this.toggleFilterPanel }
						sign={ this.state.filterPanelShow ? '-' : '+' } />
					
					<SearchBar applySearch={ this.applySearch } />
				</div>
				
				<FilterPanel
					show={ this.state.filterPanelShow }
					filters={ this.state.filters }
					toggleFilter={ this.toggleFilter }
					applyFilters={ this.applyFilters }
					resetFilters={ this.resetFilters } />
				
				<RiskPanel />
				
				<OpportunityPanel opportunities={ this.state.opportunities } />
				
				<Footer />
			</div>
		);
    }
}

export default App;
