import React from 'react';
import FilterButtonSet from './FilterButtonSet.js';
import ApplyFiltersButton from './ApplyFiltersButton.js';
import ResetFiltersButton from './ResetFiltersButton.js';

class FilterPanel extends React.Component {
	constructor(props) {
		super(props);
	}
	
	render() {
		const show = this.props.show ? ' rb-filter-panel-show' : '';
		
		return (
			<div className={ 'rb-filter-panel' + show }>
				<FilterButtonSet
					type="stage"
					filterButtons={ this.props.filters }
					toggle={ this.props.toggleFilter } />
				<FilterButtonSet
					type="sector"
					filterButtons={ this.props.filters }
					toggle={ this.props.toggleFilter } />
				
				<ApplyFiltersButton applyFilters={ this.props.applyFilters } />
				<ResetFiltersButton resetFilters={ this.props.resetFilters } />
			</div>
		);
	}
}

export default FilterPanel;